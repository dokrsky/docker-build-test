package com.example.samplehello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SampleHelloApplication {

    @Value("${spring.profiles.active}")
    private String profile;

    public static void main(String[] args) {
        SpringApplication.run(SampleHelloApplication.class, args);
    }

    @RequestMapping("/")
    public String home() {
        return "Home";
    }

    @RequestMapping("/profile")
    public String profile() {
        return profile;
    }

}
