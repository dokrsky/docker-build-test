FROM adoptopenjdk/openjdk8:alpine-slim

ENV PROFILE="local"

ARG JAR_FILE="./build/libs/*.jar"

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","-Dspring.profiles.active=${PROFILE}", "app.jar"]